-   [Podsumowanie analizy](#podsumowanie-analizy)
-   [Wykorzystane biblioteki](#wykorzystane-biblioteki)
-   [Powtarzalność](#powtarzalność)
-   [Wstępne przetworzenie danych](#wstępne-przetworzenie-danych)
    -   [Wczytanie danych](#wczytanie-danych)
    -   [Przetwarzanie brakujących
        danych](#przetwarzanie-brakujących-danych)
-   [Analiza danych](#analiza-danych)
    -   [Wstępna analiza danych](#wstępna-analiza-danych)
    -   [Rozkłady wartości atrybutów](#rozkłady-wartości-atrybutów)
    -   [Korelacje między atrybutami](#korelacje-między-atrybutami)
    -   [Zmiana rozmiaru śledzi w
        czasie](#zmiana-rozmiaru-śledzi-w-czasie)
-   [Regresor przewidujący rozmiar
    śledzia](#regresor-przewidujący-rozmiar-śledzia)
    -   [Stworzenie regresora](#stworzenie-regresora)
    -   [Analiza ważności atrybutów najlepszego znalezionego modelu
        regresji](#analiza-ważności-atrybutów-najlepszego-znalezionego-modelu-regresji)
-   [Podsumowanie](#podsumowanie)

Podsumowanie analizy
--------------------

Celem projektu była analiza przyczyn zjawiska zmniejszania się rozmiaru
śledzi wyławianych w Europie na podstawie pomiarów z ostatnich 60 lat. W
zakres projektu obejmował m.in. wstępne przetworzenie i analizę danych,
wizualizację wyników, a także stworzenie regresora dokonującego
predykcji wartości rozmiaru śledzia.

Z poniższych badań stwierdzono, iż główny wpływ na zmniejszenie się
rozmiaru śledzi ma zwiększająca się temperatura przy powierzchni wody
oraz wzrost natężenia połowów.

Wykorzystane biblioteki
-----------------------

``` r
library(ggplot2)
library(reshape2)
library(gridExtra)
library(grid)
library(plotly)
library(caret)
library(dplyr)
```

Powtarzalność
-------------

W celu zapewnienia powtarzalności eksperymentu ustawiono stały seed.

``` r
set.seed(42)
```

Wstępne przetworzenie danych
----------------------------

### Wczytanie danych

Kod pozwalający wczytać dane z pliku:

``` r
# get column classes basing on the first 1000 rows
initial <- read.table("resources/sledzie.csv", nrows = 1000, header=TRUE, sep=",")
classes <- sapply(initial, class)

# read data using colClasses argument
data <- read.table("resources/sledzie.csv", colClasses = classes, header=TRUE, sep=",")
```

Nazwy i domyślnie ustawione typy kolumn:

``` r
knitr::kable(classes)
```

|        | x       |
|--------|:--------|
| X      | integer |
| length | numeric |
| cfin1  | factor  |
| cfin2  | factor  |
| chel1  | factor  |
| chel2  | factor  |
| lcop1  | factor  |
| lcop2  | factor  |
| fbar   | numeric |
| recr   | integer |
| cumf   | numeric |
| totaln | numeric |
| sst    | factor  |
| sal    | numeric |
| xmonth | integer |
| nao    | numeric |

Podgląd danych:

``` r
knitr::kable(head(data))
```

|    X|  length| cfin1   | cfin2   | chel1   | chel2    | lcop1   | lcop2    |   fbar|    recr|       cumf|    totaln| sst           |       sal|  xmonth|  nao|
|----:|-------:|:--------|:--------|:--------|:---------|:--------|:---------|------:|-------:|----------:|---------:|:--------------|---------:|-------:|----:|
|    0|    23.0| 0.02778 | 0.27785 | 2.46875 | ?        | 2.54787 | 26.35881 |  0.356|  482831|  0.3059879|  267380.8| 14.3069330186 |  35.51234|       7|  2.8|
|    1|    22.5| 0.02778 | 0.27785 | 2.46875 | 21.43548 | 2.54787 | 26.35881 |  0.356|  482831|  0.3059879|  267380.8| 14.3069330186 |  35.51234|       7|  2.8|
|    2|    25.0| 0.02778 | 0.27785 | 2.46875 | 21.43548 | 2.54787 | 26.35881 |  0.356|  482831|  0.3059879|  267380.8| 14.3069330186 |  35.51234|       7|  2.8|
|    3|    25.5| 0.02778 | 0.27785 | 2.46875 | 21.43548 | 2.54787 | 26.35881 |  0.356|  482831|  0.3059879|  267380.8| 14.3069330186 |  35.51234|       7|  2.8|
|    4|    24.0| 0.02778 | 0.27785 | 2.46875 | 21.43548 | 2.54787 | 26.35881 |  0.356|  482831|  0.3059879|  267380.8| 14.3069330186 |  35.51234|       7|  2.8|
|    5|    22.0| 0.02778 | 0.27785 | 2.46875 | 21.43548 | 2.54787 | ?        |  0.356|  482831|  0.3059879|  267380.8| 14.3069330186 |  35.51234|       7|  2.8|

### Przetwarzanie brakujących danych

Nieznane wartości w oryginalnych danych są reprezentowane przez znak ?,
przez co są mylnie interpretowane jako typ “factor”, dlatego zamieniono
je na NA, a kolumny typu “factor” przekonwertowano na odpowiedni typ
(“numeric”).

``` r
data[data=="?"] <- NA

data$cfin1 <- as.numeric(data$cfin1)
data$cfin2 <- as.numeric(data$cfin2)
data$chel1 <- as.numeric(data$chel1)
data$chel2 <- as.numeric(data$chel2)
data$lcop1 <- as.numeric(data$lcop1)
data$lcop2 <- as.numeric(data$lcop2)
data$sst   <- as.numeric(data$sst)
```

Typy kolumn po przekształceniu:

``` r
classes <- sapply(data, class)
knitr::kable(classes)
```

|        | x       |
|--------|:--------|
| X      | integer |
| length | numeric |
| cfin1  | numeric |
| cfin2  | numeric |
| chel1  | numeric |
| chel2  | numeric |
| lcop1  | numeric |
| lcop2  | numeric |
| fbar   | numeric |
| recr   | integer |
| cumf   | numeric |
| totaln | numeric |
| sst    | numeric |
| sal    | numeric |
| xmonth | integer |
| nao    | numeric |

Podgląd danych po przekształceniu:

``` r
knitr::kable(head(data))
```

|    X|  length|  cfin1|  cfin2|  chel1|  chel2|  lcop1|  lcop2|   fbar|    recr|       cumf|    totaln|  sst|       sal|  xmonth|  nao|
|----:|-------:|------:|------:|------:|------:|------:|------:|------:|-------:|----------:|---------:|----:|---------:|-------:|----:|
|    0|    23.0|      5|     14|     20|     NA|     20|     23|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    1|    22.5|      5|     14|     20|     23|     20|     23|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    2|    25.0|      5|     14|     20|     23|     20|     23|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    3|    25.5|      5|     14|     20|     23|     20|     23|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    4|    24.0|      5|     14|     20|     23|     20|     23|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    5|    22.0|      5|     14|     20|     23|     20|     NA|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|

Analiza danych
--------------

### Wstępna analiza danych

Aby sprawdzić podstawowe statystyki, zbiór zapisano w postaci
data.frame.

``` r
df <- data.frame(data)
```

Wyznaczenie liczby kolumn i wierszy:

``` r
ncol_all <- ncol(df)
nrow_all <- nrow(df)
nrow_complete <- nrow(na.omit(df))
```

Zbiór zawiera 16 kolumn.

Całkowita liczba wierszy tego zbioru wynosi 52582, a liczba wierszy,
które nie zawierają żadnych brakujących danych jest równa 42488.

Krotki z brakującymi wartościami stanowią 19.2% całego zbioru.

Do sprawdzenia podstawowych statystyk wykorzystano funkcję `summary()`.

``` r
knitr::kable(summary(df))
```

|     |       X       |    length    |     cfin1     |     cfin2     |     chel1     |     chel2     |     lcop1     |     lcop2     |      fbar      |       recr      |       cumf      |      totaln     |      sst      |      sal      |     xmonth     |        nao       |
|-----|:-------------:|:------------:|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|:--------------:|:---------------:|:---------------:|:---------------:|:-------------:|:-------------:|:--------------:|:----------------:|
|     |    Min. : 0   |  Min. :19.0  |  Min. : 2.00  |  Min. : 2.00  |  Min. : 2.00  |  Min. : 2.00  |  Min. : 2.00  |  Min. : 2.00  |  Min. :0.0680  |  Min. : 140515  |  Min. :0.06833  |  Min. : 144137  |  Min. : 2.00  |  Min. :35.40  |  Min. : 1.000  |  Min. :-4.89000  |
|     | 1st Qu.:13145 | 1st Qu.:24.0 | 1st Qu.: 2.00 | 1st Qu.:14.00 | 1st Qu.:13.00 | 1st Qu.:15.00 | 1st Qu.:13.00 | 1st Qu.:13.00 | 1st Qu.:0.2270 | 1st Qu.: 360061 | 1st Qu.:0.14809 | 1st Qu.: 306068 | 1st Qu.:14.00 | 1st Qu.:35.51 | 1st Qu.: 5.000 | 1st Qu.:-1.89000 |
|     | Median :26290 | Median :25.5 | Median :15.00 | Median :24.00 | Median :20.00 | Median :27.00 | Median :23.00 | Median :27.00 | Median :0.3320 | Median : 421391 | Median :0.23191 | Median : 539558 | Median :25.00 | Median :35.51 | Median : 8.000 | Median : 0.20000 |
|     |  Mean :26290  |  Mean :25.3  |  Mean :16.39  |  Mean :23.64  |  Mean :23.45  |  Mean :28.07  |  Mean :23.85  |  Mean :27.82  |  Mean :0.3304  |  Mean : 520366  |  Mean :0.22981  |  Mean : 514973  |  Mean :25.45  |  Mean :35.51  |  Mean : 7.258  |  Mean :-0.09236  |
|     | 3rd Qu.:39436 | 3rd Qu.:26.5 | 3rd Qu.:28.00 | 3rd Qu.:34.00 | 3rd Qu.:36.00 | 3rd Qu.:41.00 | 3rd Qu.:35.00 | 3rd Qu.:43.00 | 3rd Qu.:0.4560 | 3rd Qu.: 724151 | 3rd Qu.:0.29803 | 3rd Qu.: 730351 | 3rd Qu.:36.00 | 3rd Qu.:35.52 | 3rd Qu.: 9.000 | 3rd Qu.: 1.63000 |
|     |  Max. :52581  |  Max. :32.5  |  Max. :40.00  |  Max. :49.00  |  Max. :49.00  |  Max. :52.00  |  Max. :49.00  |  Max. :52.00  |  Max. :0.8490  |  Max. :1565890  |  Max. :0.39801  |  Max. :1015595  |  Max. :52.00  |  Max. :35.61  |  Max. :12.000  |  Max. : 5.08000  |
|     |       NA      |      NA      |   NA’s :1581  |   NA’s :1536  |   NA’s :1555  |   NA’s :1556  |   NA’s :1653  |   NA’s :1591  |       NA       |        NA       |        NA       |        NA       |   NA’s :1584  |       NA      |       NA       |        NA        |

Brakujące wartości zastąpiono średnimi wartościami atrybutów:

``` r
for(i in 1:ncol(df)) {
  df[is.na(df[,i]), i] <- mean(df[,i], na.rm = TRUE)
}
knitr::kable(head(df))
```

|    X|  length|  cfin1|  cfin2|  chel1|    chel2|  lcop1|    lcop2|   fbar|    recr|       cumf|    totaln|  sst|       sal|  xmonth|  nao|
|----:|-------:|------:|------:|------:|--------:|------:|--------:|------:|-------:|----------:|---------:|----:|---------:|-------:|----:|
|    0|    23.0|      5|     14|     20|  28.0713|     20|  23.0000|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    1|    22.5|      5|     14|     20|  23.0000|     20|  23.0000|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    2|    25.0|      5|     14|     20|  23.0000|     20|  23.0000|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    3|    25.5|      5|     14|     20|  23.0000|     20|  23.0000|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    4|    24.0|      5|     14|     20|  23.0000|     20|  23.0000|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|
|    5|    22.0|      5|     14|     20|  23.0000|     20|  27.8161|  0.356|  482831|  0.3059879|  267380.8|   38|  35.51234|       7|  2.8|

``` r
nrow(na.omit(df)) == nrow(df)
```

    ## [1] TRUE

Wszystkie brakujące wartości zostały poprawnie zastąpione.

### Rozkłady wartości atrybutów

Stworzono wykresy przedstawiające rozkłady wartości dla wszystkich
atrybutów:

``` r
a1 <- ggplot(data = df, mapping = aes(x = df$length, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="length")

a2 <- ggplot(data = df, mapping = aes(x = df$cfin1, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="cfin1")

a3 <- ggplot(data = df, mapping = aes(x = df$cfin2, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="cfin2")

a4 <- ggplot(data = df, mapping = aes(x = df$chel1, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="chel1")

a5 <- ggplot(data = df, mapping = aes(x = df$chel2, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="chel2")

a6 <- ggplot(data = df, mapping = aes(x = df$lcop1, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="lcop1")

a7 <- ggplot(data = df, mapping = aes(x = df$lcop2, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="lcop2")

a8 <- ggplot(data = df, mapping = aes(x = df$fbar, colour = "red")) +
geom_histogram(binwidth = 0.1, show.legend=F) +
labs( y="liczba krotek", x="fbar")

a9 <- ggplot(data = df, mapping = aes(x = df$recr, colour = "red")) +
geom_histogram(binwidth = 100000, show.legend=F) +
labs( y="liczba krotek", x="recr")

a10 <- ggplot(data = df, mapping = aes(x = df$cumf, colour = "red")) +
geom_histogram(binwidth = 0.01, show.legend=F) +
labs( y="liczba krotek", x="cumf")

a11 <- ggplot(data = df, mapping = aes(x = df$totaln, colour = "red")) +
geom_histogram(binwidth = 10000, show.legend=F) +
labs( y="liczba krotek", x="totaln")
    
a12 <- ggplot(data = df, mapping = aes(x = df$sst, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="sst")

a13 <- ggplot(data = df, mapping = aes(x = df$sal, colour = "red")) +
geom_histogram(binwidth = 0.01, show.legend=F) +
labs( y="liczba krotek", x="sal")

a14 <- ggplot(data = df, mapping = aes(x = df$xmonth, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="xmonth")

a15 <- ggplot(data = df, mapping = aes(x = df$nao, colour = "red")) +
geom_histogram(binwidth = 1, show.legend=F) +
labs( y="liczba krotek", x="nao")

grid.arrange(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, nrow = 5)
```

![](README_files/figure-markdown_github/gridplots-1.png)

### Korelacje między atrybutami

Stworzenie macierzy korelacji:

``` r
cor_mat <- round(cor(subset(df,select=-c(X))),2)
```

Najpierw za pomocą funkcji `hclust()` uporządkowano macierz korelacji.
Takie uporządkowanie pozwala na czytelniejsze przedstawienie korelacji
oraz ułatwia znajdowanie ukrytych zależności pomiędzy atrybutami.

Jako odległość między zmiennymi przyjęto wartość korelacji pomiędzy
zmiennymi.

``` r
dd <- as.dist((1-cor_mat)/2)
dd[is.na(dd)] <- 0
dd[is.nan(dd)] <- 0
hc <- hclust(dd)
cor_mat <- cor_mat[hc$order, hc$order]
```

Usunięcie z macierzy korelacji redundantnych informacji:

``` r
cor_mat[lower.tri(cor_mat)]<- NA
```

Pokazanie macierzy korelacji:

``` r
melted_cor_mat <- melt(cor_mat, na.rm = TRUE)

ggplot(data = melted_cor_mat, 
 aes(Var2, Var1, fill = value)) +
 geom_tile(color = "white") +
 scale_fill_gradient2(low = "blue", high = "red", mid = "white", 
   midpoint = 0, limit = c(-1,1), space = "Lab", 
   name="Korelacja atrybutów") +
 theme_minimal() + 
 theme(
    axis.text.x = element_text(angle = 45, vjust = 1, size = 10, hjust = 1),
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
    panel.grid.major = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank(),
    axis.ticks = element_blank(),
   ) +
 coord_fixed()
```

![](README_files/figure-markdown_github/unnamed-chunk-16-1.png) Atrybuty
są ze sobą skorelowane w umiarkowanym stopniu. Na powyższym wykresie
można szybko łatwo zauważyć, które atrybuty są skorelowane z innymi, a
które pozostają w mniejszym lub większym stopniu niezależne od innych
zmiennych. Przykładowo, atrybut `xmonth` ma bardzo niskie współczynniki
korelacji z każdym z atrybutów (z wyjątkiem siebie oczywiście).
Posortowanie korelacji między różnymi parami atrybutów:

``` r
upper_tri <- cor_mat
# set value for the same attributes as NA
upper_tri[lower.tri(upper_tri,diag = TRUE)]<- NA
# save as data.frame
df_upper_tri <- melt(upper_tri, na.rm = TRUE)
# add abs value column
df_upper_tri$abs <- abs(df_upper_tri$value)
# sort descending by abs value
df_abs_cor_mat <- df_upper_tri[order(-df_upper_tri$abs),1:3]

head(df_abs_cor_mat)
```

    ##       Var1  Var2 value
    ## 192   fbar  cumf  0.82
    ## 160  chel2 lcop2  0.75
    ## 183 totaln  cumf -0.71
    ## 96   chel1 lcop1  0.70
    ## 128    sst   nao  0.55
    ## 168 totaln  fbar -0.51

Najbardziej skorelowaną parą różnych atrybutów jest `fbar` i `cumf`.
Atrybuty `fbar` (natężenie połowów w regionie) i `cumf` (łączne roczne
natężenie połowów w regionie) z samej nazwy sugerują dużą zależność
między nimi, dlatego badanie ich niekoniecznie musi być interesujące.

W tym miejscu warto sprawdzić z jakimi atrybutami najbardziej
skorelowana jest długość śledzia, czyli `length`:

``` r
length_cor <- df_abs_cor_mat[df_abs_cor_mat$Var1 == "length" | df_abs_cor_mat$Var2 == "length",1:3]
length_cor
```

    ##       Var1   Var2 value
    ## 110 length    sst -0.44
    ## 125 length    nao -0.26
    ## 170 length   fbar  0.25
    ## 95  length  lcop1  0.21
    ## 80  length  chel1  0.17
    ## 200 length  cfin1  0.14
    ## 63  totaln length  0.10
    ## 61     sal length  0.03
    ## 215 length  cfin2  0.03
    ## 155 length  lcop2 -0.02
    ## 62    recr length -0.01
    ## 64  xmonth length  0.01
    ## 140 length  chel2  0.01
    ## 185 length   cumf  0.01

Długość śledzia `length` dosyć mocno skorelowana jest z: \* `sst`
temperatura przy powierzchni wody \[°C\], \* `nao` oscylacja
północnoatlantycka \[mb\]. \* `fbar` natężenie połowów w regionie
(ułamek pozostawionego narybku\]), \* `chel1` i `lcop1` i `cfin1`
(dostępności planktonu gat. 1),

Długość śledzia nie wykazuje prawie żadnej korelacji z: \* `cumf`
(łączne roczne natężenie połowów w regionie), \* `chel2` i `lcop2` i
`cfin2` (dostępności planktonu gat. 2), \* `recr` (roczny narybek -
liczba śledzi), \* `xmonth` (miesiąc połowu), \* `sal` (poziom zasolenia
wody).

``` r
p1 <- ggplot(df, aes(x = df$sst, y=df$length)) + 
geom_count(col="tomato", show.legend=F) +
geom_smooth(method="loess", se=F) + 
labs(title="length(sst)", 
       y="length", 
       x="sst")

p2 <- ggplot(df, aes(x = df$nao, y=df$length)) + 
geom_count(col="tomato", show.legend=F) +
geom_smooth(method="loess", se=F) + 
labs(title="length(nao)", 
       y="length", 
       x="nao")

p3 <- ggplot(df, aes(x = df$fbar, y=df$length)) + 
geom_count(col="tomato", show.legend=F) +
geom_smooth(method="loess", se=F) + 
labs(title="length(fbar)", 
       y="length", 
       x="fbar")

p4 <- ggplot(df, aes(x = df$chel1, y=df$length)) + 
geom_count(col="tomato", show.legend=F) +
geom_smooth(method="loess", se=F) + 
labs(title="length(chel1)", 
       y="length", 
       x="chel1")

p5 <- ggplot(df, aes(x = df$lcop1, y=df$length)) + 
geom_count(col="tomato", show.legend=F) +
geom_smooth(method="loess", se=F) + 
labs(title="length(lcop1)", 
       y="length", 
       x="lcop1")

p6 <- ggplot(df, aes(x = df$cfin1, y=df$length)) + 
geom_count(col="tomato", show.legend=F) +
geom_smooth(method="loess", se=F) + 
labs(title="length(cfin1)", 
       y="length", 
       x="cfin1")
```

``` r
grid.arrange(p1, p2, nrow = 1)
```

![](README_files/figure-markdown_github/unnamed-chunk-20-1.png)

``` r
grid.arrange(p3, p4, nrow = 1)
```

![](README_files/figure-markdown_github/unnamed-chunk-21-1.png)

``` r
grid.arrange(p5, p6, nrow = 1)
```

![](README_files/figure-markdown_github/unnamed-chunk-22-1.png)

### Zmiana rozmiaru śledzi w czasie

Zmianę rozmiaru śledzi w czasie zaprezentowano za pomocą poniższego
interaktywnego wykresu:

``` r
reduced_df <- df[seq(1, nrow(df), 10), ] #take every tenth row
p <- ggplot(reduced_df, aes(X, length, alpha = 0.3)) +
  labs(x = "Numer złowionego śledzia", y = "Długość śledzia")+
  geom_line() + geom_smooth(method = "loess", formula = y ~ x, se = FALSE, colour = "red")
p
```

![](README_files/figure-markdown_github/unnamed-chunk-23-1.png)

Na wykresie wyraźnie widać, że od jakiegoś czasu rozmiar śledzi maleje.

Regresor przewidujący rozmiar śledzia
-------------------------------------

### Stworzenie regresora

Na początku usunięto z danych sztuczną kolumnę zawierającą indeks wpisu:

``` r
predict_df <- subset(df, select=-c(X))
```

Następnie zbiór podzielono na treningowy(75%) oraz testowy(25%), za
pomocą biblioteki caret wykonano dostrojenie algorytmu regresji liniowej
oraz odpornej regresji liniowej z wykorzystaniem k-fold walidacji
(przyjęto k=10).

``` r
train_idx <- createDataPartition(predict_df$length, p = 0.75, list = FALSE)
train <- predict_df[train_idx,]
test  <- predict_df[-train_idx,]

fit_ctrl <- trainControl(method = "repeatedcv", number = 10, repeats = 2)

fit_lm <- train(length ~ ., data = train, method = "lm", trControl = fit_ctrl)
fit_rlm <- train(length ~ ., data = train, method = "rlm", trControl = fit_ctrl)
```

Po dostrojeniu i wyuczeniu modeli sprawdzono ich skuteczność na nowych,
wcześniej nie widzianych przez nich danych:

``` r
pred_lm <- predict(fit_lm, newdata = test)
pred_rlm <- predict(fit_rlm, newdata = test)
```

Wyniki dla modelu regresji liniowej:

``` r
postResample(pred_lm, test$length)
```

    ##     RMSE Rsquared      MAE 
    ## 1.342457 0.338997 1.070639

Wyniki dla modelu odpornej regresji liniowej:

``` r
postResample(pred_rlm, test$length)
```

    ##      RMSE  Rsquared       MAE 
    ## 1.3424669 0.3389696 1.0703641

Oba modele regresji cechują się niskimi miarami błędu regresji, co
świadczy o ich dobrym przewidywaniu rozmiaru śledzia.

### Analiza ważności atrybutów najlepszego znalezionego modelu regresji

Zgodnie z wcześniejszymi przypuszczeniami, temperatura przy powierzchni
wody okazała się być najważniejszym atrybutem przy predykcji. Zaraz po
nim, drugim najistotniejszym atrybutem jest `fbar`, natężenie połowów w
regionie. Innym ważnym atrybutem zauważonym również podczas analizy
korelacji jest jeden z określających dostępność planktonu gat. 1 -
`cfin1`.

``` r
ggplot(varImp(fit_rlm))
```

![](README_files/figure-markdown_github/unnamed-chunk-29-1.png)

Podsumowanie
------------

Podsumowując badania, można stwierdzić, że długość śledzia zależy przede
wszystkim od temperatury przy powierzchni wody oraz natężenia połowów w
regionie. Dostępność planktonu gat.1 jest również ważna, jednak w nieco
mniejszym stopniu.

Główną przyczyną malenia rozmiaru śledzia jest zatem prawdopodobnie
wzrost temperatury przy powierzchni wody (ujemna korelacja) oraz
zwiększone natężenie połowów w danym regionie.
